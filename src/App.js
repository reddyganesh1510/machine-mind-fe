import logo from './logo.svg';
import './App.css';
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Home from './components/Home';
import Transcribe from './components/Transcribe';
import Summarize from './components/Summarize';
import Query from './components/Query';
import { useState } from 'react';
import {ToastContainer} from 'react-toastify'

import "react-toastify/dist/ReactToastify.css";

function App() {
  return (
    <>
<ToastContainer />
   

    <BrowserRouter>
    <Routes>
      <Route path="/" element={<Home />}/>
        <Route path="/transcribe" element={<Transcribe />} />
        <Route path="/summarize" element={<Summarize />} />
        <Route path="/query" element={<Query />} />
    </Routes>
  </BrowserRouter></>
    
  );
}

export default App;
