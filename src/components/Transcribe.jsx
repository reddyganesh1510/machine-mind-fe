import React, { useState ,useEffect } from 'react'
import axios from 'axios'
import { toast } from 'react-toastify';
import Navbar from './Navbar'
import Hero from './Hero'
import Responsebox from './Responsebox'



function Transcribe() {
  const [text,setText]= useState("");
  useEffect(() => {
    axios.get(`http://localhost:8080/`).then(res=>{
      if(res.status == 200){
        toast.success("File uploaded successfully")
      }else{
        toast.error("Error")
      }
    }).catch(err=>{
      toast.error("Error")
    
    })
  },[]);
  return (
    <div>
      <Navbar />
    <div className="mx-auto mt-40 p-5 max-w-screen-xl px-4  lg:flex lg:h-[65vh] flex flex-col shadow-[0_8px_30px_rgb(0,0,0,0.12)] ">
    <Responsebox screen="transcribe" text="text"/>

</div>
    </div>
  )
}

export default Transcribe
